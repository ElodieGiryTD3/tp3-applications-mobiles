package fr.uavignon.ceri.tp3;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;
import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;

public class MainActivity extends AppCompatActivity {
    //final private WeatherRepository repo = WeatherRepository.get(getApplication());
    private CityDao cityDao;
    private List<City> allCities;
    private MutableLiveData<City> selectedCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {

            //EG : déclaration de la variable clé de l'API
            /*
            final MutableLiveData<WeatherResult> result = new MutableLiveData<>();
            List<City> listeVille;

            WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(getApplication());
            cityDao = db.cityDao();
            allCities = cityDao.getSynchrAllCities();

            for(int i=0; i<allCities.size(); i++)
                Log.d("", String.valueOf(allCities.get(i)));

             */
            //repo.loadWeatherAllCities();
/*
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Interrogation à faire du service web",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
 */
        }

        return super.onOptionsItemSelected(item);
    }
}