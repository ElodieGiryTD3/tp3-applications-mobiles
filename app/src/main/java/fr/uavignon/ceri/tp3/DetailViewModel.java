package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;
import fr.uavignon.ceri.tp3.data.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;

public class DetailViewModel extends AndroidViewModel {
    final private WeatherRepository repo = WeatherRepository.get(getApplication());
    final MediatorLiveData<WeatherResult> results = new MediatorLiveData<>();
    private LiveData<WeatherResult> lastResult;

    public static final String TAG = DetailViewModel.class.getSimpleName();

    private WeatherRepository repository;
    private MutableLiveData<City> city;

    public DetailViewModel (Application application) {
        super(application);
        repository = WeatherRepository.get(application);
        city = new MutableLiveData<>();
    }

    public void setCity(long id) {
        repository.getCity(id);
        city = repository.getSelectedCity();
    }

    LiveData<City> getCity() {
        return city;
    }

    public void loadCity(City city) {
        Log.d(TAG, "city name = "+city.getName());

        if (lastResult != null) {
            results.removeSource(lastResult);
        }
        //lastResult = repo.loadWeatherCity(city);
        repo.loadWeatherCity(city);

        results.addSource(lastResult, weather -> {
            //ArrayList<WeatherResponse> rows = new ArrayList<>();
            WeatherResponse rows = new WeatherResponse();

            if (weather!= null) {
                //city.setHumidity(rows.main.humidity);
            }
            results.postValue(
            new WeatherResult(false, rows, null));

        });
    }
}