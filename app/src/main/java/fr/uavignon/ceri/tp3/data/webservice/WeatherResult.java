package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherResponse;

//EG : Ajout de la classe permettant de remplir un objet city avec les réponses du service web

public class WeatherResult {
    public final boolean isLoading;
    public final WeatherResponse weatherResponse;
    public final Throwable error;

    public WeatherResult(boolean isLoading, WeatherResponse weatherResponse, Throwable error) {
        this.isLoading = isLoading;
        this.weatherResponse = weatherResponse;
        this.error = error;
    }

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo){
        cityInfo.setName(weatherInfo.name);
        Map<String, String> mapCountryNameToCode=cityInfo.getCountryNameToCodeMap();
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setLastUpdate(weatherInfo.dt);
    }
}
