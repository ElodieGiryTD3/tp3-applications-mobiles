package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;

    private CityDao cityDao;

    private static volatile WeatherRepository INSTANCE;

    //EG : déclaration de la variable de l'interface pour appeler le service web
    private final OWMInterface api;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {

        //EG : déclaration de l'url du service web et de son API
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(OWMInterface.class);


        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }

    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // EG : ajout de la fonction pour appeler le service web (Callback) et
    // récupérer les réponses en asynchrone (en arrière plan)

    public void loadWeatherCity(City city) {
        //EG : déclaration de la variable clé de l'API
        String cle = "fa5161019671c27456320130f8af06fc";
        final MutableLiveData<WeatherResult> result = new MutableLiveData<>();

        result.setValue(new WeatherResult(true, null, null));

        //Appel du service API
        api.getForecast(city.getName(), cle).enqueue(
                new Callback<WeatherResponse>() {
                    //Reception réponse = Callback
                    @Override
                    public void onResponse(Call<WeatherResponse> call,
                                           Response<WeatherResponse> response) {

                        Log.d(TAG, "RESPONSE DE LOAD WEATHER CITY =" + response.toString());

                        WeatherResponse weatherResponse = new WeatherResponse(response.body().weather, response.body().main, response.body().wind,  response.body().clouds,
                                response.body().sys, response.body().name, response.body().dt );

                        WeatherResult weatherResult = new WeatherResult(false, weatherResponse, null);
                        weatherResult.transferInfo(weatherResponse,city);

                        Log.d(TAG, "CITY NAME         = " + city.getName());
                        Log.d(TAG, "CITY COUNTRY      = " + city.getCountry());
                        Log.d(TAG, "CITY ICON         = " + city.getIconUri());
                        Log.d(TAG, "CITY TEMP         = " + (int) Math.round(city.getTemperature())+" °C");
                        Log.d(TAG, "CITY HUMIDITY     = " + city.getHumidity()+" %");
                        Log.d(TAG, "CITY VENT COMBINE = " + city.getFullWind());
                        Log.d(TAG, "CITY CLOUD        = " + city.getCloudiness()+" %");
                        Log.d(TAG, "CITY UPDATE TIME  = " + city.getStrLastUpdate());

                        //Appel du databaseWriteExecutor et du dao pour MAJ attributs de type LiveData, selected city et leurs observateurs
                        updateCity(city);

                        weatherResult = new WeatherResult(false, weatherResponse, null);
                        result.postValue(weatherResult);
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        result.postValue(new WeatherResult(false, null, t));

                        if (call.isCanceled()) {
                            Log.d(TAG, "Call was cancelled forcefully");
                        } else {
                            Log.d(TAG,"Network Error :: " + t.getLocalizedMessage());
                        }
                    }
                });
    }

    public void loadWeatherAllCities() {
        //EG : déclaration de la variable clé de l'API
        final MutableLiveData<WeatherResult> result = new MutableLiveData<>();
        List<City> listeVille;

        listeVille=cityDao.getSynchrAllCities();

        for(int i=0; i<listeVille.size(); i++)
            Log.d(TAG, String.valueOf(listeVille.get(i)));

    }

}
