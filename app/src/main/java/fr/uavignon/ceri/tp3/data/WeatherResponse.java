package fr.uavignon.ceri.tp3.data;
import java.util.ArrayList;
import java.util.List;

//EG : Ajout de la classe description de réponses de l'API
public class WeatherResponse {
    public List<Weather> weather=null;
    public Main main=null;
    public Wind wind=null;
    public Clouds clouds=null;
    public Sys sys=null;
    public String name=null;
    public long dt=0;

    public WeatherResponse(List<Weather> weather, Main main, Wind wind, Clouds clouds, Sys sys,  String name, long dt) {
        this.weather = weather;
        this.main = main;
        this.wind = wind;
        this.clouds = clouds;
        this.sys = sys;
        this.name = name;
        this.dt=dt;
    }

    public WeatherResponse() {
    }

    public static class Weather{
        public String description=null;
        public String icon=null;
    }

    public static class Main{
        public Float temp=0.0f;
        public Integer humidity=0;
    }

    public static class Wind {
        public Float speed=0.0f;
        public Integer deg=0;
    };

    public static class Clouds {
        public Integer all=null;
    }

    public static class Sys {
        public String country=null;
    }
}
